package libraryproject;

import java.sql.SQLException;
import java.util.Scanner;

public class LibraryProject {

    private Admin a;
    private Statistics c;

    public LibraryProject() throws SQLException {
        a = new Admin();
        c = new Statistics();
    }

    public static void main(String[] args) throws SQLException {
        LibraryProject lp = new LibraryProject();

        int menuNumber;

        for (int i = 0;; i++) {
            lp.printMenu();
            Scanner sc = new Scanner(System.in);
            menuNumber = sc.nextInt();
            if (menuNumber < 1 || menuNumber > 8) {
                break;
            } else {
                lp.selectChoosenAction(menuNumber);
            }
        }
        
        
                 
    }

        


    
// 1. menu
    private void addUserInLibrary() throws SQLException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Add meg a neved!");
        String name = sc.nextLine();
        System.out.println("Add meg a lakcímed!");
        String adress = sc.nextLine();
        System.out.println("Add meg az e-mail címed!");
        String email = sc.nextLine();
        a.addUser(name, adress, email);

    }
// 2. menu
    private void newBookIntoLibrary() throws SQLException {
        System.out.println("Új könyv bevitele:");
        Scanner sc = new Scanner(System.in);
        System.out.println("Add meg a címet!");
        String title = sc.nextLine();
        System.out.println("Add meg a kategóriát");
        String category = sc.nextLine();
        System.out.println("Add meg a szerzőt!");
        String author = sc.nextLine();
        a.addBook(title, category, author);
    }
// 3. menu
    private void existingBookToInventory() throws SQLException {
        System.out.println("Meglévő könyv bevitele:");
        Scanner sc = new Scanner(System.in);
        System.out.println("Add meg a darabszámot");
        int n = sc.nextInt();
        System.out.println("Add meg a címet");
        String title = sc.nextLine();
        System.out.println("Add meg a szerzőt!");
        String author = sc.nextLine();
        a.addBookToInventory(n, title, author);
    }
// 4. menu
    private void rentalBook() throws SQLException {
        System.out.println("Kölcsönzés:");
        Scanner sc = new Scanner(System.in);
        System.out.println("Add meg az e-mail címed!");
        String email = sc.nextLine();
        System.out.println("Add meg a címet");
        String title = sc.nextLine();
        System.out.println("Add meg a szerzőt!");
        String author = sc.nextLine();
        a.addRental(email, title, author);
    }
// 5. menu
    private void searchByTitle() throws SQLException {
        System.out.println("Keresés cím alapján:");
        Scanner sc = new Scanner(System.in);
        System.out.println("Add meg a címet vagy annak egy részét!");
        String title = sc.nextLine();
        a.searchByTitle(title);
    }
// 6. menu
    private void searcByAuthor() throws SQLException {
        System.out.println("Keresés szerző alapján:");
        Scanner sc = new Scanner(System.in);
        System.out.println("Add meg az írót vagy amire emlékszel a nevéből...");
        String author = sc.nextLine();
        a.searchByAuthor(author);
    }
// 7. menu
    private void booksByUser() throws SQLException {
        System.out.println("Könyvek a felhasználóknál");
        c.usersAndRentedBooks();
    }
// 8. menu    
    private void booksBySingleUser() throws SQLException {
        System.out.println("Adott felhasználónál lévő könyvek: Id--i  email--e ");
        Scanner sc = new Scanner(System.in);
        String chooseIdOrEmail = sc.nextLine();
        if (chooseIdOrEmail.equals("i")) {
            System.out.println("Add meg a felhazsnáló azonosítóját:");
            int id = sc.nextInt();
            c.getUsersRentedBooks(id);
        } else {
            System.out.println("Add meg a felhazsnáló email-jét:");
            String userEmail = sc.nextLine();
            c.getUsersRentedBooks(userEmail);
        }
    }

    private void printMenu() {
        System.out.println("");
        System.out.println("Válassz a következő lehetőségek közül:");
        System.out.println("- - - - - - - - - - - - - - - - - -");
        System.out.println("1. Felhasználó hozzáadása ");
        System.out.println("2. Új könyv hozzáadása");
        System.out.println("3. Meglévő könyv hozzáadása");
        System.out.println("4. Könyv kölcsönzése");
        System.out.println("5. Cím alapján keres");
        System.out.println("6. Szerző alapján keres");
        System.out.println("7. Felhasználóknál lévő könyvek");
        System.out.println("8. Adott felhasználónál lévő könyvek");
        System.out.println("9. Vége");
    }

    private void selectChoosenAction(int number) throws SQLException {
        LibraryProject lpSelect = new LibraryProject();
        if (number == 1) {
            lpSelect.addUserInLibrary();
        }
        if (number == 2) {
            lpSelect.newBookIntoLibrary();
        }
        if (number == 3) {
            lpSelect.existingBookToInventory();
        }
        if (number == 4) {
            lpSelect.rentalBook();
        }
        if (number == 5) {
            lpSelect.searchByTitle();
        }
        if (number == 6) {
            lpSelect.searcByAuthor();
        }
        if (number == 7) {
            lpSelect.booksByUser();
        }
        if (number == 8) {
            lpSelect.booksBySingleUser();
        }
    }

}
