/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import com.mysql.cj.xdevapi.Result;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author én
 */
public class Statistics {

    private Url url;

    public Statistics() throws SQLException {
        url = new Url();
    }

    public void usersAndRentedBooks() throws SQLException {

        String usersAndRentedBooksSql = "SELECT rental.user_id, username, title from rental join inventory join book JOIN user\n"
                + "on rental.inventory_id=inventory.inventory_id and inventory.book_id=book.book_id and rental.user_id = user.user_id\n"
                + "where return_date is null\n"
                + "ORDER BY rental.user_id";
        PreparedStatement usersAndRentedBooksPs = url.getConn().prepareStatement(usersAndRentedBooksSql);
        ResultSet results = usersAndRentedBooksPs.executeQuery();
        while (results.next()) {
            System.out.println(" - ID: " + results.getString("user_id") + " Kölcsönző neve: "
                    + results.getString("username") + " Könyv címe: " + results.getString("title"));
        }
    }

    public void getUsersRentedBooks(String email) throws SQLException {

        String userExistsSql = "SELECT * FROM user WHERE email = ?";
        PreparedStatement userExistsPs = url.getConn().prepareStatement(userExistsSql, Statement.RETURN_GENERATED_KEYS);
        userExistsPs.setString(1, email);
        ResultSet userResult = userExistsPs.executeQuery();

        if (userResult.next()) {

            String getRentedBooksSql = "SELECT title from rental join inventory join book JOIN user\n"
                    + "on rental.inventory_id=inventory.inventory_id and inventory.book_id=book.book_id and rental.user_id = user.user_id\n"
                    + "where return_date is null and user.email = ?\n"
                    + "ORDER BY rental.user_id";

            PreparedStatement getRentedBooksPs = url.getConn().prepareStatement(getRentedBooksSql);
            getRentedBooksPs.setString(1, email);
            ResultSet results = getRentedBooksPs.executeQuery();

            while (results.next()) {
                System.out.println(" - " + results.getString("title"));
            }
        } else {
            System.out.println("A megadott email címhez nem tartozik felhasználó!");
        }
    }

    public void getUsersRentedBooks(int user_id) throws SQLException {

        String userExistsSql = "SELECT * FROM user WHERE user_id = ?";
        PreparedStatement userExistsPs = url.getConn().prepareStatement(userExistsSql, Statement.RETURN_GENERATED_KEYS);
        userExistsPs.setInt(1, user_id);
        ResultSet userResult = userExistsPs.executeQuery();

        if (userResult.next()) {

            String getRentedBooksSql = "SELECT title from rental join inventory join book JOIN user\n"
                    + "on rental.inventory_id=inventory.inventory_id and inventory.book_id=book.book_id and rental.user_id = user.user_id\n"
                    + "where return_date is null and rental.user_id = ?\n"
                    + "ORDER BY rental.user_id";

            PreparedStatement getRentedBooksPs = url.getConn().prepareStatement(getRentedBooksSql);
            getRentedBooksPs.setInt(1, user_id);
            ResultSet results = getRentedBooksPs.executeQuery();

            while (results.next()) {
                System.out.println(" - " + results.getString("title"));
            }
        } else {
            System.out.println("Nem szerepel a rendszerben felhasználó ezen az ID-n!");
        }
    }

    public void getMostPopularBook() throws SQLException {

        String mostPopularBookSql = "SELECT author, title from book where";
        PreparedStatement mostPopularBookPs = url.getConn().prepareStatement(mostPopularBookSql);
        ResultSet result = mostPopularBookPs.executeQuery();

    }

}
