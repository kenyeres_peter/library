package libraryproject;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

public class Admin {

    private Url url;

    public Admin() throws SQLException {
        url = new Url();
    }

    public void addUser(String name, String adress, String email) throws SQLException {

        String emailExistSql = "SELECT * FROM user WHERE email=?";
        PreparedStatement emailExistsPs = url.getConn().prepareStatement(emailExistSql);
        emailExistsPs.setString(1, email);

        ResultSet emailResult = emailExistsPs.executeQuery();

        if (emailResult.next()) {
            System.out.println("Ezen az email címen már van regisztrált felhasználó!");
        } else {
            String newUserSql = "INSERT INTO user (username, email, address) VALUES (?, ?, ?)";
            PreparedStatement newUserPs = url.getConn().prepareStatement(newUserSql);
            newUserPs.setString(1, name);
            newUserPs.setString(2, email);
            newUserPs.setString(3, adress);
            newUserPs.executeUpdate();
            System.out.println("Sikeres regisztráció.");
        }

    }

    public void addBook(String title, String category, String author) throws SQLException {
        //megnézzük van e ilyen könyv
        ResultSet bookResult = checkBook(title, author);
        int bookId;
        //ha van akkor betesszük az inventoryba a book_id alapján
        if (bookResult.next()) {
            bookId = bookResult.getInt("book_id");
            insertBookToInventory(bookId);
            System.out.println("Könyv példány a raktárba helyezve.");
        } else {
            //ha nincs akkor betesszük a book táblába
            String insertBookSql = "INSERT INTO book (title,category,author) VALUES (?,?,?)";
            PreparedStatement insertBookPs = url.getConn().prepareStatement(insertBookSql);
            insertBookPs.setString(1, title);
            insertBookPs.setString(2, category);
            insertBookPs.setString(3, author);
            insertBookPs.executeUpdate();
            //hozzáadjuk az inventoryhoz
            addBookToInventory(1, title, author);
            System.out.println("A könyv bekerült a rendszerbe.");
        }
    }

    //x példány hozzáadása az inventoryhoz
    public void addBookToInventory(int n, String title, String author) throws SQLException {

        ResultSet bookResult = checkBook(title, author);
        int bookId;
        if (bookResult.next()) {
            for (int i = 0; i < n; i++) {
                bookId = bookResult.getInt("book_id");
                insertBookToInventory(bookId);
            }
        } else {
            System.out.println("error...");
        }
    }

    public void addRental(String email, String title, String author) throws SQLException {
        //csekkoljuk a usert
        ResultSet userResult = checkUser(email);
        int userId;
        if (userResult.next()) {
            //ha van user csekkoljuk a könyvet
            userId = userResult.getInt("user_id");
            ResultSet bookResult = checkBook(title, author);
            int bookId;
            //ha van könyv akkor elkérünk egy inventory id-t
            if (bookResult.next()) {
                bookId = bookResult.getInt("book_id");
                ResultSet invIdResult = getInventoryId(bookId);
                int invId;
                //betesszük az rentalba  táblába a kölcsönzést
                if (invIdResult.next()) {
                    invId = invIdResult.getInt(1);
                    String insertRentalSql = "INSERT INTO rental (inventory_id, user_id) VALUES (?,?)";
                    PreparedStatement insertRentalPs = url.getConn().prepareStatement(insertRentalSql);
                    insertRentalPs.setInt(1, invId);
                    insertRentalPs.setInt(2, userId);
                    insertRentalPs.executeUpdate();
                    System.out.println("Kölcsönzés sikeres volt.");
                }
            } else {
                System.out.println("Nincs ilyen könyv a rendszerben.");
            }
        } else {
            System.out.println("Nincs ilyen felhasználó a rendszerben.");
        }
    }

    public void updateRentedBookReturnDate(String email, String title, String author) throws SQLException {
        //csekkoljuk a usert
        ResultSet userResult = checkUser(email);
        int userId;
        if (userResult.next()) {
            //ha van user csekkoljuk a könyvet
            userId = userResult.getInt("user_id");
            ResultSet bookResult = checkBook(title, author);
            int bookId;
            //ha van könyv akkor elkérünk egy inventory id-t
            if (bookResult.next()) {
                bookId = bookResult.getInt("book_id");
                ResultSet invIdResult = getInventoryId(bookId);
                int invId;
                //update-eljük a return date-et
                if (invIdResult.next()) {
                    invId = invIdResult.getInt(1);
                    String insertRentalSql = "UPDATE rental SET return_date = now() where user_id = ? AND inventory_id = ?";
                    PreparedStatement insertRentalPs = url.getConn().prepareStatement(insertRentalSql);
                    insertRentalPs.setInt(1, userId);
                    insertRentalPs.setInt(2, invId);
                    insertRentalPs.executeUpdate();
                    System.out.println("A könyv visszahozása megtörtént.");
                }
            } else {
                System.out.println("Nincs ilyen könyv a rendszerben.");
            }
        } else {
            System.out.println("Nincs ilyen felhasználó a rendszerben.");
        }
    }

    public void searchByAuthor(String author) throws SQLException {
        String authorSql = "Select * FROM book WHERE author = ? OR author LIKE ?";
        PreparedStatement authorPs = url.getConn().prepareStatement(authorSql);
        String modAuthor = "%" + author + "%";
        authorPs.setString(1, author);
        authorPs.setString(2, modAuthor);
        ResultSet authorResult = authorPs.executeQuery();
        int count = 0;
        System.out.println("Találat: ");
        while (authorResult.next()) {
            System.out.println(authorResult.getString("author") + " - " + authorResult.getString("title"));
            count++;
        }
        if (count < 1) {
            System.out.println("Nincs ilyen szerző az adatbázisban");
        }

    }

    public void searchByTitle(String title) throws SQLException {
        String titleSql = "Select * FROM book WHERE title LIKE ? OR title LIKE ?";
        PreparedStatement titlePs = url.getConn().prepareStatement(titleSql);
        String modTitle = "%" + title + "%";
        String modTitleSec = title + "%";
        titlePs.setString(1, modTitle);
        titlePs.setString(2, modTitleSec);
        ResultSet titleResult = titlePs.executeQuery();
        int count = 0;
        System.out.println("Találat: ");
        while (titleResult.next()) {
            System.out.println("Cím: " + titleResult.getString("title") + " szerző: " + titleResult.getString("author"));
            count++;
        }
        if (count < 1) {
            System.out.println("Nincs ilyen könyv az adatbázisban");
        }
    }

    private ResultSet checkUser(String email) throws SQLException {
        String userExistsSql = "SELECT * FROM user WHERE email = ?";
        PreparedStatement userExistsPs = url.getConn().prepareStatement(userExistsSql, Statement.RETURN_GENERATED_KEYS);
        userExistsPs.setString(1, email);
        ResultSet userResult = userExistsPs.executeQuery();
        return userResult;
    }

    private ResultSet checkBook(String title, String author) throws SQLException {
        String bookExistsSql = "SELECT * FROM book WHERE title LIKE ? OR title LIKE ? AND author = ? OR author LIKE ?";
        PreparedStatement bookExistsPs = url.getConn().prepareStatement(bookExistsSql, Statement.RETURN_GENERATED_KEYS);
        String modTitle = "%" + title + "%";
        String modTitleSec = title + "%";
        String modAuthor = "%" + author + "%";
        bookExistsPs.setString(1, modTitle);
        bookExistsPs.setString(2, modTitleSec);
        bookExistsPs.setString(3, author);
        bookExistsPs.setString(4, modAuthor);
        ResultSet bookResult = bookExistsPs.executeQuery();
        return bookResult;
    }

    private ResultSet getInventoryId(int bookId) throws SQLException {
        String inventoryIdSql = "SELECT inventory_id FROM inventory WHERE book_id = ?";
        PreparedStatement inventoryIdPs = url.getConn().prepareStatement(inventoryIdSql, Statement.RETURN_GENERATED_KEYS);
        inventoryIdPs.setInt(1, bookId);
        inventoryIdPs.setInt(1, bookId);
        ResultSet invIdResult = inventoryIdPs.executeQuery();
        return invIdResult;
    }

    private void insertBookToInventory(int bookId) throws SQLException {
        String bookInsertInventory = "INSERT INTO inventory (book_id) VALUES (?)";
        PreparedStatement bookToInventoryPs = url.getConn().prepareStatement(bookInsertInventory);
        bookToInventoryPs.setInt(1, bookId);
        bookToInventoryPs.executeUpdate();
    }
}
