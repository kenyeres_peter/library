/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author én
 */
public class Url {
    private String url = "jdbc:mysql://localhost/library_project?serverTimezone=UTC";
    private Connection conn;

    public Url() throws SQLException {                  
        conn = DriverManager.getConnection(url, "root", "root1234");
    }

    public String getUrl() {
        return url;
    }

    public Connection getConn() {
        return conn;
    }
    
    
    
}
